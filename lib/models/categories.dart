class Categories {
  final bool value;
  final List<CategoriesData> catData;

  Categories({this.value, this.catData});

  factory Categories.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['data'] as List;
    List<CategoriesData> data =
        list.map((i) => CategoriesData.fromJson((i))).toList();
    return Categories(
      value: parsedJson['value'],
      catData: data
    );
  }
}

class CategoriesData {
  final int catId;
  final String catName;
  final String catImage;

  CategoriesData({this.catId, this.catName, this.catImage});

  factory CategoriesData.fromJson(Map<String, dynamic> parsedJson) {
    return CategoriesData(
        catId: parsedJson['id'],
        catImage: parsedJson['image'],
        catName: parsedJson['name']);
  }
}

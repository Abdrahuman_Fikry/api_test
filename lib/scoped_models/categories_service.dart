import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:api_test/models/categories.dart';

class CategoriesModel extends Model {
  Categories _categories;
  Dio dio = Dio();

  Future<Response<dynamic>> getCats() async {
    dio.options.baseUrl = 'url';
    return await dio.get('/url');
  }

  Future<Categories> loadCategoriesResponse() async {
    var jsonString, jsonResponse;
    jsonString = await getCats();
    jsonResponse = json.decode(jsonString.toString());
    if (jsonString.statusCode >= 200 && jsonString.statusCode < 300) {
      _categories = Categories.fromJson(jsonResponse);
      return _categories;
    } else {
      return _categories;
    }
  }
}

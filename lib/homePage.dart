import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:api_test/models/categories.dart';
import 'package:api_test/scoped_models/mainModel.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Categories categories=Categories();
  MainModel model= MainModel();
  @override
  void initState(){
    _getCats();
    super.initState();
  }
  bool _loading= false;
  _getCats() async{
    setState(() {
      _loading= true;
    });
    Categories _cats= await model.loadCategoriesResponse();
    setState(() {
      categories=_cats;
      print(categories.catData[0].catName);
      _loading= false;

    });
  }
  Widget _buildItem(String name) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child:Row(children: <Widget>[
        Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10), color: Colors.green),
          child: Center(child: Text(name),),
        ),
      ],)
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('الرئيسية'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.search),
          onPressed: () {},
        ),
      ),
      body: _loading? Center(child: CircularProgressIndicator(),): ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: categories.catData.length,
        itemBuilder: (context, i) {
          return _buildItem(
            categories.catData[i].catName
          );
        },
      ),
    );
  }
}
